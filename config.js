export default {
  server: {
    ip: 'arc.liberdus.com',
    port: 4000
  },
  version: '1.0.0'
}
